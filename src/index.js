import React from 'react';
import ReactDOM from 'react-dom';
import AvatarPicker from "./js/components/AvatarPicker";

ReactDOM.render(<AvatarPicker />, document.getElementById('root'));

